# moonbook

moonscript and markdown based inline executed notebooks

think Jupyter for moonscript without the bloated runtime.

## setup

* install the [requirements](requirements)
* compile: `moonc init.moon`

## usage

* run: `luajit init.lua input_file.moonb` -> creates `input_file.html`
* see [`test.moonb`](test.moonb) for features + usage
